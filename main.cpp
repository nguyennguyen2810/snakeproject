#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"
using namespace olc;
//test
#include <math.h>
using namespace std;

enum class menu {MAIN, CLASSIC};

class Snake : public olc::PixelGameEngine
{
public:
	Snake()
	{
		sAppName = "Snake";
	}

private:
	wstring map;
	int mapWidth;
	int mapHeight;
	int tileSize = 32;
	int headPosX;
	int headPosY;
	int velX = 0;
	int velY = 0;
	int fruitPosX;
	int fruitPosY;
	int score;
	vector<pair<int, int>> segments;
	int length = 0;
	bool gameOver;
	float nextMove = 0;
	float speed = 0.1f; // maxSpeed = 0
	float time;
	menu menuScreen;

public:
	bool OnUserCreate() override
	{
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		map += L"....................";
		mapWidth = 20;
		mapHeight = 20;
		headPosX = mapWidth / 2;
		headPosY = mapHeight / 2;
		fruitPosX = rand() % mapWidth;
		fruitPosY = rand() % mapHeight;
		map[fruitPosY * mapWidth + fruitPosX] = L'*';
		score = 0;
		gameOver = 0;
		segments.push_back(make_pair(headPosX, headPosY));

		menuScreen = menu::MAIN;
		return true;
	}

	bool OnUserUpdate(float fElapsedTime) override
	{
		// Utility Lamdas
		auto GetTile = [&](int x, int y)
		{
			if (x >= 0 && x < mapWidth && y >= 0 && y < mapHeight)
				return map[y * mapWidth + x];
			else
				return L' ';
		};
		auto SetTile = [&](int x, int y, wchar_t c)
		{
			if (x >= 0 && x < mapWidth && y >= 0 && y < mapHeight)
				return map[y * mapWidth + x] = c;
		};
		switch (menuScreen)
		{
		case menu::MAIN:
			Clear(BLANK);
			DrawString(ScreenWidth() / 4, 10, "Snake Project", WHITE, 5);
			DrawString(ScreenWidth() / 3, ScreenHeight() / 4, "New game", WHITE, 5);
			if (IsFocused())
			{
				if (GetMouse(0).bPressed || GetKey(SPACE).bPressed)
				{
					menuScreen = menu::CLASSIC;
					Clear(BLANK);
				}
			}
			break;
		case menu::CLASSIC:
			if (IsFocused())
			{
				if (GetKey(UP).bPressed || GetKey(W).bPressed && velY == 0)
				{
					velX = 0;
					velY = -1;
				}
				if (GetKey(DOWN).bPressed || GetKey(S).bPressed && velY == 0)
				{
					velX = 0;
					velY = 1;
				}
				if (GetKey(LEFT).bPressed || GetKey(A).bPressed && velX == 0)
				{
					velX = -1;
					velY = 0;
				}
				if (GetKey(RIGHT).bPressed || GetKey(D).bPressed && velX == 0)
				{
					velX = 1;
					velY = 0;
				}
			}
			time += fElapsedTime;
			if (time >= nextMove)
			{
				Clear(olc::BLANK);

				// Handle Input
				pair<int, int> prePos = make_pair(headPosX, headPosY);
				headPosX += velX;
				headPosY += velY;

				if (headPosY >= mapHeight) headPosY = 0;
				if (headPosY < 0) headPosY = (mapHeight - 1);
				if (headPosX >= mapWidth) headPosX = 0;
				if (headPosX < 0) headPosX = (mapWidth - 1);



				// Collision
				if (headPosX == fruitPosX && headPosY == fruitPosY)
				{
					score = score + 1;
					length++;
					SetTile(fruitPosX, fruitPosY, L'.');
					fruitPosX = rand() % mapWidth;
					fruitPosY = rand() % mapHeight;
					SetTile(fruitPosX, fruitPosY, L'*');
					segments.push_back(prePos);
				}

				for (int i = 1; i < length; i++)
				{
					if (headPosX == segments[i].first && headPosY == segments[i].second)
						gameOver = true;
				}

				for (int i = length; i > 0; i--)
				{
					segments[i].first = segments[i - 1].first;
					segments[i].second = segments[i - 1].second;
				}
				segments[0].first = headPosX;
				segments[0].second = headPosY;

				// Draw Map
				for (int x = 0; x < mapWidth; x++)
				{
					for (int y = 0; y < mapHeight; y++)
					{
						wchar_t tileChar = GetTile(x, y);
						switch (tileChar)
						{
						case L'.':
							FillRect(x * tileSize, y * tileSize, tileSize, tileSize, WHITE);
							break;
						case L'*':
							FillRect(x * tileSize, y * tileSize, tileSize, tileSize, RED);
							break;
						default:
							break;
						}
					}
				}

				// Draw Snake
				FillRect(headPosX * tileSize, headPosY * tileSize, tileSize, tileSize, VERY_DARK_GREEN);
				for (int i = 1; i <= length; i++)
				{
					FillRect(segments[i].first * tileSize, segments[i].second * tileSize, tileSize, tileSize, GREEN);
				}

				//DrawString(0, tileSize * (mapHeight), "Score", WHITE);
				nextMove += speed;
				DrawString(0, mapHeight * tileSize, "Score: " + to_string(score), WHITE, 3);
			}
			if (gameOver)
			{
				menuScreen = menu::MAIN;
				gameOver = false;
			}
			break;
		default:
			break;
		}
		return true;
	}
};

int main()
{
	Snake game;
	if (game.Construct(1280, 720, 1, 1))
		game.Start();
	return 0;
}

